import os

folder_path = "DERNIER_RUN"

table = []
for filename in os.listdir(folder_path):

    file_path = os.path.join(folder_path, filename)
    with open(file_path, "r") as file:
        content = file.readlines()

        for line in content:
            words = line.split()

            if len(words) == 3 and words[1] == "all" and words[0] == "MAgP":
                value = words[2]
                #print(filename + " score : " + value)
                row = {"filename": filename, "value": float(value)}
                table.append(row)

table = sorted(table, key=lambda x: x["value"], reverse=True)
print("{:<20s} {:<10s}".format("Filename", "Value"))
print("-" * 32)
for row in table:
    print("{:<20s} {:<10.6f}".format(row["filename"], row["value"]))
                # do something with the value
