import networkx as nx
import pandas as pd
import os

# Load the training data into a Pandas dataframe
training_data = pd.read_csv("task1/2016-12-1_train.csv")

## Create a graph from the training data
G = nx.from_pandas_edgelist(training_data, "Source", "Target", ["value", "nb_transactions"])

# Use the Common Neighbors method for link prediction
def predict_link(G, u, v):
    if u not in G or v not in G:
        return 0, 0
    neighbors = set(G[u]).intersection(G[v])
    num_neighbors = len(neighbors)
    return num_neighbors, num_neighbors

# Load the test data into a Pandas dataframe
test_data = pd.read_csv("task1/2016-12-1_test.csv")

# Predict the links in the test data
predictions = []
for index, row in test_data.iterrows():
    u = row["Source"]
    v = row["Target"]
    value, nb_transactions = predict_link(G, u, v)
    temp = 0
    if (nb_transactions != 0) :
        temp = 1
    predictions.append((u, v, 0, temp))

# Store the predictions in a csv file
prediction_output = pd.DataFrame(predictions, columns=["Source", "Target", "Predicted Value", "Predicted nb_transactions"])
directory = "prediction"
if not os.path.exists(directory):
    os.makedirs(directory)

prediction_output.to_csv("prediction/predictionSimple_output.csv", index=False)
