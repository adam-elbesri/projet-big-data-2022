import pandas as pd
import os
import numpy as np
folder = 'task2'

# list all the csv files in the folder
files = [f for f in os.listdir(folder) if f.endswith('.csv')]

# create an empty dataframe with columns for the file name and statistics
columns = ['file_name'] + ['mean_value', 'mean_nb_transactions', 'min_value', 'max_value', 'min_nb_transactions', 'max_nb_transactions', 'unique_sources', 'unique_targets']
result = pd.DataFrame(columns=columns)

# loop over the files
for file in files:
    file_path = os.path.join(folder, file)
    df = pd.read_csv(file_path)
    mean_value = df['value'].mean()
    mean_nb_transactions = df['nb_transactions'].mean()
    min_value = df['value'].min()
    max_value = df['value'].max()
    unique_sources = df['Source'].nunique()
    unique_targets = df['Target'].nunique()
    min_nb_transactions = df['nb_transactions'].min()
    max_nb_transactions = df['nb_transactions'].max()
    data = [file, mean_value, mean_nb_transactions, min_value, max_value, min_nb_transactions, max_nb_transactions, unique_sources, unique_targets]
    result = result.append(pd.Series(data, index=result.columns), ignore_index=True)

print(result)
# flag any file with a mean value greater than 10,000
result['abnormal'] = result['mean_nb_transactions'] > 9.5
result['abnormal_greater'] = result['unique_sources'] > result['unique_targets']
mean_value_mean = result['mean_value'].mean()
mean_value_std = result['mean_value'].std()
mean_nb_transactions_mean = result['mean_nb_transactions'].mean()
mean_nb_transactions_std = result['mean_nb_transactions'].std()

# find the 5 files that have abnormal data
abnormal_files = result[(np.abs(result['mean_value'] - mean_value_mean) > 3*mean_value_std) |
                        (np.abs(result['mean_nb_transactions'] - mean_nb_transactions_mean) > 3*mean_nb_transactions_std)][:5]
print(abnormal_files)
abnormal_files.to_csv("abnormal_files.csv")
result.sort_values('min_value', ascending=True)
result.to_csv("task2_stats.csv")