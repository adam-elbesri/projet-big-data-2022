import pandas as pd
import matplotlib.pyplot as plt
import os


df = pd.read_csv('task2_adam.csv')
# Convert the "date" column to a datetime object
df["date"] = pd.to_datetime(df["date"])
# Extract the year, month, and day from the datetime object
df["year"] = df["date"].dt.year
df["month"] = df["date"].dt.month
df["day"] = df["date"].dt.day

#print(df)

# Group the data by source, target, year, and month
grouped = df.groupby(by=["Source", "Target", "year", "month"])

# Calculate the sum of "value" and "nb_transactions" for each group
grouped_sum = grouped.agg({"value": "sum", "nb_transactions": "sum"})

# Reset the index of the grouped data frame
grouped_sum = grouped_sum.reset_index()

# Plot the sum of "value" and "nb_transactions" for each group over time (months)
plt.plot(grouped_sum["month"], grouped_sum["value"], label="Value")
plt.plot(grouped_sum["month"], grouped_sum["nb_transactions"], label="Number of Transactions")
plt.legend()
plt.show()

# Group the transactions by "source", "target", "date", and "value"
grouped = df.groupby(["Source", "Target", "value", "nb_transactions"])

# Count the number of transactions in each group
grouped_count = grouped.size().reset_index(name="count")

counted = grouped_count
grouped_count.to_csv('count.csv')

# Filter the groups that have a count greater than 1
repeated_transactions = grouped_count[grouped_count["count"] > 10]

# Reset the index of the filtered data frame
repeated_transactions = repeated_transactions.reset_index(drop=True)
print(repeated_transactions)